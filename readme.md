# InstaForms

A drop in package to add standard forms to your Laravel 5.2 project.

Available forms;

* Contact form
* Newsletter signup form

By default, all form submissions will be emailed to the default email address in `config/mail.php`, unless you specify differently in the InstaForms config file (see Optional Installation).

InstaForms will also save to your database of choice provided you follow the steps in the Optional Installation section.

All forms ship with Bootstrap 3 styling. Please see Optional Installation for how to amend this.

# Installation

Add the repository to your `composer.json` file

```
    "repositories": [
        {
            "type": "vcs",
            "url": "https://bitbucket.org/danwithams/insta-forms.git"
        }
    ],
```

Require the package in your project by adding it your `composer.json` file;

```
"danwithams/insta-forms": "dev-master"
```

And running the composer update command for this package;

```
composer update danwithams/insta-forms
```

Add the ServiceProvider to the providers array in `config/app.php`

```
DanWithams\InstaForms\InstaFormsServiceProvider::class
```

Add the custom form builder Facade;
```
'InstaForm' => DanWithams\InstaForms\Facades\InstaForm::class
```

# Optional Installation options

To change the packages configurations, publish the config file to your `config/` directory;

```
php artisan vendor:publish --tag="config"
```

To use the database saving feature of InstaForms, publish the migration file and run the new migrations;

```
php artisan vendor:publish --tag="migrations"
php artisan migrate
```

It is even possible to customise the views of the forms, is it possible to publish the views with the following;

```
php artisan vendor:publish --tag="views"
```

# Usage

Simply use the `InstaForm` Facade in any* of your views to make one of the available forms. The examples below assumes your are using the blade templating engine;

```
{!! InstaForm::make('newsletter-form') !!}

{!! InstaForm::make('contact-form') !!}
```

*Important!! Since Laravel 5.2 session and default laravel error flashing has been moved to an OPTIONAL middleware. Ensure that the view you are calling is called through a Controller under the 'web' middleware group. See middle group documentation here; (https://laravel.com/docs/5.2/middleware).