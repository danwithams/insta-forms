<?php

namespace DanWithams\InstaForms\Services;

use DanWithams\InstaForms\Models\InstaFormsMessage;
use Mail;
use View;
use Log;
use Schema;

class FormService
{
    public function __construct()
    {

    }

    public function process($request)
    {
        // Try and email the result of the form.
        $hasSentEmail = $this->sendEmail($request);
        
        // Try and save the result of the form to the database.
        $hasSavedToDatabase = $this->saveToDatabase($request);
        
        // Since you can configure either Email and/or Database for InstaForms
        // as long as one of these has happened successfully, we don't need
        // indicate there was a problem.
        return $hasSentEmail || $hasSavedToDatabase;
    }
    
    public function saveToDatabase($request)
    {
        if ($this->getHasDatabase()) {
            // Save to the database
            $instaFormsMessage = new InstaFormsMessage;
            $instaFormsMessage->form = $request->form_name;
            $instaFormsMessage->response = json_encode($request->except(['form_name', '_token']));

            return $instaFormsMessage->save();
        } else {
            return false;
        }
    }

    public function sendEmail($request)
    {
        // Check that we have somewhere to send an email notification
        $email = $this->getEmailTo();

        // Check that we have an email address to send to.
        if ($email['address'] && $email['name']) {

            $emailViewPath = sprintf('insta-forms::emails.%s', $request->form_name);

            // Check the template exists
            if (View::exists($emailViewPath)) {
                Mail::send($emailViewPath, [ 'email' => $email, 'request' => $request->except('_token', 'form_name'), 'form_name' => $request->form_name ], function ($mailer) use ($email) {
                    // Use the same email/name as the 'From' field as this is familiar
                    $mailer->from($email['address'], $email['name']);
                    
                    // Set the 'To' field to the configured receiver
                    $mailer->to($email['address'], $email['name'])->subject('New message from InstaForms!');
                });
                
                // Everything went smoothly.
                return true;
            } else {
                // No email template found, log the error to Laravels system log.
                Log::error(sprintf('[InstaForms] - No email template called "%s" to send to for form "%s"', $emailViewPath, $request->form_name));
                return false;
            }
        } else {
            // No email address to send to, log the error to Laravels system log.
            Log::error(sprintf('[InstaForms] - No email address to send to for form "%s"', $request->form_name));
            return false;
        }
    }

    private function getEmailTo()
    {
        // Return the config for where to send the email to

        // Check instaForms config first of all.
        $instaFormsEmailTo = config('insta-forms.contact');
        
        if ($instaFormsEmailTo['address'] && $instaFormsEmailTo['name']) {
            return $instaFormsEmailTo;    
        }

        // Check Laravels mail config secondly.
        $laravelEmailTo = config('mail.from');
        
        if ($laravelEmailTo['address'] && $laravelEmailTo['name']) {
            return $laravelEmailTo;    
        }
        
        // Nothing to use, return nulls which will fail.
        return ['address' => null, 'name' => null];
    }

    private function getHasDatabase()
    {
        return Schema::hasTable('insta_forms_messages');
    }

    /**
     * @return string
     */
    public function getRouteUri($handle = false)
    {
        // Get the bespokse route for InstaForms - this helps prevent clashing with other routes.
        return md5('insta-forms-unique-uri') . (($handle) ? '/' . $handle : '');
    }
}