<?php

namespace DanWithams\InstaForms\Services;

use DanWithams\InstaForms\Services\FormService;
use Session;
use View;
use Log;

class ViewManager
{
    private $formService;
    
    public function __construct()
    {
        $this->formService = new FormService;
    }

    /**
     * Get the registered name of the component.
     * @return string
     */
    public function make($handle)
    {
        $viewPath = sprintf('insta-forms::forms.%s', $handle);
        
        // Check to see if the view exists.
        if (View::exists($viewPath)) {
            // Create the view with the default variables.
            $view = View::make($viewPath);
            
            // Check if we have a success 
            $success = Session::get('insta_form_success');
            $message = Session::get('insta_form_message', 'Form submitted successfully.');

            $view
                ->with('csrfToken', csrf_token())
                ->with('uri', $this->formService->getRouteUri($handle))
                ->with('formName', $handle)
                ->with('instaFormSuccess', ($success && $success == $handle) ? $message : false)
                ->render();
            
            // Because we want to be able to use multiple forms on one page,
            // we need to flush the sections clean so we can repopulate the
            // 'content' section (and also so we don't interfere with other
            // 'content' sections.
            View::flushSections();
                
            return $view;
        } else {
            // Log error to the Laravel system log.
            Log::error(sprintf('[InstaForms] - Unable to find form "%s".', $viewPath));
            // Inform user gracefully.
            return 'There was a problem loading the form. Please contact the site administrator.';
        }
    }
}