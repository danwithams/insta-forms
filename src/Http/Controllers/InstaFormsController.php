<?php
    
namespace DanWithams\InstaForms\Http\Controllers;

use DanWithams\InstaForms\Http\Controllers\BaseController;
use DanWithams\InstaForms\Http\Requests\ContactRequest;
use DanWithams\InstaForms\Http\Requests\NewsletterRequest;
use DanWithams\InstaForms\Services\FormService;
use Redirect;


class InstaFormsController extends BaseController
{
    private $formService;
    
    public function __construct()
    {
        $this->formService = new FormService;
    }
    
    public function postContactForm(ContactRequest $request)
    {
        // Process the request
        $results = $this->formService->process($request);
        
        if ($results) {
            return Redirect::back()
                ->with('insta_form_success', $request->form_name)
                ->with('insta_form_message', 'You have been signed up to the newsletter.');
        } else {
            return Redirect::back()
                ->withErrors(['Something went wrong with the form. Please try again later'], $request->form_name);
        }
    }

    public function postNewsletterForm(NewsletterRequest $request)
    {
        // Process the request
        $results = $this->formService->process($request);

        if ($results) {
            return Redirect::back()
                ->with('insta_form_success', $request->form_name)
                ->with('insta_form_message', 'You have been signed up to the newsletter.');
        } else {
            return Redirect::back()
                ->withErrors(['Something went wrong with the form. Please try again later'], $request->form_name);
        }
    }
}





