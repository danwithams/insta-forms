<?php namespace DanWithams\InstaForms\Http\Requests;

use DanWithams\InstaForms\Http\Requests\Request;

class ContactRequest extends Request {

    protected $errorBag = 'contact-form';

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|min:3',
            'last_name' => 'required|min:3',
	        'email' => 'required|email',
	        'message' => 'required|min:15',
        ];
    }

    /**-
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

}