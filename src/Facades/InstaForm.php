<?php

namespace DanWithams\InstaForms\Facades;

use Illuminate\Support\Facades\Facade;

class InstaForm extends Facade
{
    /**
     * Get the registered name of the component.
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'DanWithams\InstaForms\Services\ViewManager';
    }
}