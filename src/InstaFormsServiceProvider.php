<?php

namespace DanWithams\InstaForms;

use DanWithams\InstaForms\Services\FormService;
use Illuminate\Support\ServiceProvider;
use Hash;
use Route;

class InstaFormsServiceProvider extends ServiceProvider
{
    protected $uri;
    
    /**
     * Boot service provider.
     */
    public function boot()
    {
        // Set the publish directories for config.
        $this->publishes([
            $this->getConfigPath() => config_path('insta-forms.php'),
        ], 'config');

        // Set a new custom directory for view loading.    
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'insta-forms');
    
        // Set the publish directories for views
        $this->publishes([
            __DIR__ . '/../resources/views' => resource_path('views/vendor/insta-forms'),
        ], 'views');

        // Set the publish directories for migrations.
        $this->publishes([
            __DIR__.'/../database/migrations/' => database_path('migrations'),
        ], 'migrations');

        // Load the routes file if we can.
        if (!$this->app->routesAreCached()) {
            require __DIR__.'/../../../../app/Http/routes.php';
        }
    }
    
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        // Merge the config into the system
        $this->mergeConfigFrom(
            $this->getConfigPath(), 'insta-forms'
        );
        
        $formService = new FormService;
        
        Route::group(['middleware' => ['web']], function () use ($formService) {
            Route::controller($formService->getRouteUri(), 'DanWithams\InstaForms\Http\Controllers\InstaFormsController');
        });
    }

    /**
     * @return string
     */
    protected function getConfigPath()
    {
        return __DIR__ . '/../config/insta-forms.php';
    }
}
