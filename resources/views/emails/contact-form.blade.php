
@extends('insta-forms::master-email')

@section('content')

<table>
    <tr>
        <td>Name</td>
        <td>{{ $request['first_name'] }} {{ $request['last_name'] }}</td>
    </tr>
    <tr>
        <td>Email</td>
        <td>{{ $request['email'] }}</td>
    </tr>
    <tr>
        <td>Message</td>
        <td>{{ $request['message'] }}</td>
    </tr>
</table>

@endsection