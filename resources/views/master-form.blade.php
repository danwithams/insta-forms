<form method="post" action="/{{ $uri }}">
    <input type="hidden" name="form_name" value="{{ $formName }}" />
    <input type="hidden" name="_token" value="{{ $csrfToken }}" />
    @yield('content')
</form>