
@extends('insta-forms::master-form')

@section('content')

<h1>Newsletter form</h1>

@if (count($errors->getBag($formName)->all()) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->getBag($formName)->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if ($instaFormSuccess)
    <div class="alert alert-success">
    {{ $instaFormSuccess }}
    </div>
@endif

<div class="form-inline">
    <label for="{{ $formName }}_email">Email:</label>
    <input id="{{ $formName }}_email" class="form-control" type="text" name="email" value="{{ old('email') }}" />
    <button class="btn btn-primary" type="submit">Sign up now</button>
</div>

@endsection