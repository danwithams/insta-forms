
@extends('insta-forms::master-form')

@section('content')

<h1>Contact form</h1>

@if (count($errors->getBag($formName)->all()) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->getBag($formName)->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if ($instaFormSuccess)
    <div class="alert alert-success">
    {{ $instaFormSuccess }}
    </div>
@endif

<label for="{{ $formName }}_first_name">First name:</label>
<input id="{{ $formName }}_first_name" class="form-control" type="text" name="first_name" value="{{ old('first_name') }}" />


<label for="l{{ $formName }}_ast_name">Last name:</label>
<input id="{{ $formName }}_last_name" class="form-control" type="text" name="last_name" value="{{ old('last_name') }}" />

<label for="{{ $formName }}_email">Email:</label>
<input id="{{ $formName }}_email" class="form-control" type="email" name="email" value="{{ old('email') }}" />

<label for="{{ $formName }}_message">Message:</label>
<textarea id="{{ $formName }}_message" class="form-control" name="message">{{ old('message') }}</textarea>

<br />
<button class="btn btn-primary" type="submit">Send message</button>

@endsection